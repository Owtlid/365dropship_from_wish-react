import "./Aside.css";
import DropshipLogo from "./DropshipLogo";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import SpeedIcon from "@material-ui/icons/Speed";
import MarkunreadMailboxIcon from "@material-ui/icons/MarkunreadMailbox";
import AssignmentIcon from "@material-ui/icons/Assignment";
import SwapHorizontalCircleIcon from "@material-ui/icons/SwapHorizontalCircle";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import { Link, useLocation } from "react-router-dom";
import { useEffect, useState } from "react";

const Aside = () => {
  const location = useLocation();
  const [status, setStatus] = useState({
    user: false,
    dashboard: false,
    catalog: false,
    cart: false,
  });

  useEffect(() => {
    switch (location.pathname) {
      case "/user":
        setStatus({
          dashboard: false,
          catalog: false,
          cart: false,
          user: true,
        });
        break;
      case "/dashboard":
        setStatus({
          catalog: false,
          cart: false,
          user: false,
          dashboard: true,
        });
        break;
      case "/catalog":
        setStatus({
          user: false,
          dashboard: false,
          cart: false,
          catalog: true,
        });
        break;
      case "/cart":
        setStatus({
          user: false,
          dashboard: false,
          catalog: false,
          cart: true,
        });
        break;
      default:
        setStatus({ ...status });
        break;
    }
    // eslint-disable-next-line
  }, [location.pathname]);

  return (
    <aside className="aside">
      <DropshipLogo />
      <div className="aside__nav">
        <Link to="/user">
          <AccountCircleIcon
            fontSize="large"
            color={status.user ? "primary" : "secondary"}
          />
        </Link>
        <Link to="/dashboard">
          <SpeedIcon
            fontSize="default"
            color={status.dashboard ? "primary" : "secondary"}
          />
        </Link>
        <Link to="/catalog">
          <FormatListBulletedIcon
            fontSize="default"
            color={status.catalog ? "primary" : "secondary"}
          />
        </Link>
        <MarkunreadMailboxIcon fontSize="default" color="secondary" />
        <Link to="/cart">
          <ShoppingCartIcon
            fontSize="default"
            color={status.cart ? "primary" : "secondary"}
          />
        </Link>
        <AssignmentTurnedInIcon fontSize="default" color="secondary" />
        <SwapHorizontalCircleIcon fontSize="default" color="secondary" />
        <AssignmentIcon fontSize="default" color="secondary" />
      </div>
    </aside>
  );
};

export default Aside;
