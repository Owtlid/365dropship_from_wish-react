import "./Modal.css";
import { useState, useEffect } from "react";
import { useHistory } from "react-router";
import { Auth, Cart, Products } from "../../API/Request";
import { Dialog, DialogTitle, Button, Snackbar } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { useDispatch, useSelector } from "react-redux";
import {
  changeModalIdAction,
  changeSingleProductAction,
} from "../../Store/Products/ProductsActions";
import MuiAlert from "@material-ui/lab/Alert";

const Modal = () => {
  const id = useSelector((state) => state.products.modalId);
  const [open, setOpen] = useState(false);
  const history = useHistory();
  const [modalStatus, setModalStatus] = useState(false);
  const dispatch = useDispatch();
  const singleItem = useSelector(
    (state) => state.products.singleProductDetails
  );

  useEffect(() => {
    try {
      Products.getSingleProduct(id).then((res) => {
        dispatch(changeSingleProductAction(res));
        setModalStatus(true);
        history.push(`/catalog/${id}`);
      });
    } catch (err) {
      alert("INVALID ID");
    }
  }, [id, history, dispatch]);

  const close = () => {
    setModalStatus(false);
    dispatch(changeModalIdAction(null));
    history.push("/catalog");
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  const addOnClick = () => {
    Auth.checkTokenValidity();
    Cart.addItemToCart(id).then(() => {
      setOpen(true);
    });
  };

  return (
    <Dialog
      onClose={close}
      aria-labelledby="customized-dialog-title"
      open={modalStatus}
      fullWidth
      maxWidth="md"
    >
      <div className="modal">
        <div className="modal__photo">
          <div className="photo__price">
            <ul className="photo__price-list">
              <li>{singleItem.price}$</li>
              <li>{singleItem.price}$</li>
              <li>{singleItem.price}$</li>
            </ul>
          </div>
          <div className="photo__slideshow">
            <div className="slideshow__primary">
              <img src={singleItem.imageUrl} alt="productImage" />
            </div>
            <ul className="slideshow__secondary">
              <li>
                <img src={singleItem.imageUrl} alt="productImage" />
              </li>
              <li>
                <img src={singleItem.imageUrl} alt="productImage" />
              </li>
              <li>
                <img src={singleItem.imageUrl} alt="productImage" />
              </li>
            </ul>
          </div>
        </div>
        <div className="modal__title">
          <DialogTitle onClose={close}>{singleItem.title}</DialogTitle>
          <div className="title__button">
            <Button variant="contained" color="primary" onClick={addOnClick}>
              Add To Cart
            </Button>
          </div>
          <div className="title__tabs">
            <div className="title__tabs-item"> Product Details </div>
            <div className="title__tabs-item"> Shipping Rates </div>
            <div className="title__tabs-item"> Available Options </div>
          </div>
          <div className="title_desc">
            <p>{singleItem.description}</p>
          </div>
        </div>
      </div>
      <div className="closeBtn" onClick={close}>
        <CloseIcon fontSize="large" color="secondary" />
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="success">
          Added Successfully!
        </Alert>
      </Snackbar>
    </Dialog>
  );
};
export default Modal;
