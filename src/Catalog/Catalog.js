import "./Catalog.css";
import Filter from "./CatalogFilter/Filter";
import CatalogHeader from "./CatalogHeader/CatalogHeader";
import CatalogProduct from "./CatalogProduct/CatalogProduct";
import { Grid } from "@material-ui/core";
import { useEffect } from "react";
import Modal from "./Modal/Modal";
import { Products } from "../API/Request";
import { useParams } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import {
  addProductsAction,
  changeModalIdAction,
} from "../Store/Products/ProductsActions";
import AddProd from "./AdminModals/AddProd";
import EditProd from "./AdminModals/EditProd";
import Loading from "../Assets/loading.gif";

const Catalog = () => {
  const slug = useParams();
  const dispatch = useDispatch();
  const productList = useSelector((state) => state.products.productsForDisplay);
  const modalId = useSelector((state) => state.products.modalId);

  useEffect(() => {
    try {
      Products.getProducts().then((item) => {
        dispatch(addProductsAction(item));
      });
    } catch (err) {
      console.error(err);
    }
  }, [dispatch]);

  useEffect(() => {
    if (slug.slug > 0) {
      dispatch(changeModalIdAction(slug.slug));
    }
  }, [slug, dispatch]);

  return (
    <>
      {modalId && <Modal />}
      <div className="catalog">
        <div className="catalog__filter">
          <Filter />
        </div>
        <div className="catalog__main">
          <CatalogHeader />
          <Grid container justify="flex-start" className="ctn">
            {productList.length ? (
              productList.map((item, i) => (
                <Grid item lg={3} md={4} sm={6} xl={3} xs={12} key={i}>
                  <CatalogProduct
                    id={item.id}
                    title={item.title}
                    price={item.price}
                    image={item.imageUrl}
                  />
                </Grid>
              ))
            ) : (
              <div className="loading">
                <img src={Loading} alt="ldng" />
              </div>
            )}
          </Grid>
        </div>
        <AddProd />
        <EditProd />
      </div>
    </>
  );
};

export default Catalog;
