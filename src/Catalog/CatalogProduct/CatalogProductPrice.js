import { useDispatch } from "react-redux";
import { changeModalIdAction } from "../../Store/Products/ProductsActions";
import "./CatalogProductPrice.css";
const CatalogProductPrice = ({ price, id }) => {
  const dispatch = useDispatch();

  const modalIdSetter = () => {
    dispatch(changeModalIdAction(id));
  };

  return (
    <div className="catalog__price" onClick={modalIdSetter}>
      {price}$
    </div>
  );
};
export default CatalogProductPrice;
