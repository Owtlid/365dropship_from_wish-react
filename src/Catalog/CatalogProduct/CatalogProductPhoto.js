import { useDispatch } from "react-redux";
import { changeModalIdAction } from "../../Store/Products/ProductsActions";
import "./CatalogProductPhoto.css";
const CatalogProductPhoto = ({ image, id }) => {
  const dispatch = useDispatch();

  const modalIdSetter = () => {
    dispatch(changeModalIdAction(id));
  };

  return (
    <div className="catalog__photo" onClick={modalIdSetter}>
      <img src={image} alt="product" />
    </div>
  );
};
export default CatalogProductPhoto;
