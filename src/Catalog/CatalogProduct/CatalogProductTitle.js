import { useDispatch } from "react-redux";
import { changeModalIdAction } from "../../Store/Products/ProductsActions";
import "./CatalogProductTitle.css";
const CatalogProductTitle = ({ title, id }) => {
  const dispatch = useDispatch();

  const modalIdSetter = () => {
    dispatch(changeModalIdAction(id));
  };

  return (
    <div className="catalog__title" onClick={modalIdSetter}>
      {title}
    </div>
  );
};
export default CatalogProductTitle;
