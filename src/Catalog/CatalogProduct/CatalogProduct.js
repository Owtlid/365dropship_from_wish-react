import "./CatalogProduct.css";
import CatalogProductSelector from "./CatalogProductSelector";
import CatalogProductPhoto from "./CatalogProductPhoto";
import CatalogProductTitle from "./CatalogProductTitle";
import CatalogProductPrice from "./CatalogProductPrice";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";

const CatalogProduct = ({ id, title, price, image }) => {
  const [select, setSelect] = useState(false);
  const prodList = useSelector((state) => state.products.productsForDisplay);

  useEffect(() => {
    for (let i = 0; i < prodList.length; i++) {
      if (prodList[i].id === id && prodList[i].selected) {
        setSelect(true);
      } else if (prodList[i].id === id && !prodList[i].selected) {
        setSelect(false);
      }
    }
  }, [prodList, id]);

  return (
    <div
      className={
        "catalog__product" + [select ? " catalog__product-selected" : ""]
      }
    >
      <div className="prod_sel">
        <CatalogProductSelector id={id} />
      </div>
      <div className="prod__det">
        <CatalogProductPhoto image={image} id={id} />
        <CatalogProductTitle title={title} id={id} />
        <CatalogProductPrice price={price} id={id} />
      </div>
    </div>
  );
};
export default CatalogProduct;
