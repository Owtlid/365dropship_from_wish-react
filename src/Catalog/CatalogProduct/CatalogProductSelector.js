import "./CatalogProductSelector.css";
import { Checkbox } from "@material-ui/core";
import { Button } from "@material-ui/core";
import { useEffect, useState } from "react";
import { Admin, Auth, Cart, Products } from "../../API/Request";
import { useDispatch, useSelector } from "react-redux";
import {
  addProductsAction,
  changeEditProdModalId,
  selectProdAction,
} from "../../Store/Products/ProductsActions";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";

const CatalogProductSelector = ({ id }) => {
  const [open, setOpen] = useState(false);
  const [select, setSelect] = useState(false);
  const dispatch = useDispatch();
  const prodList = useSelector((state) => state.products.productsForDisplay);

  const selectedProd = (arr, id, status) => {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].id === id) {
        arr[i] = {
          ...arr[i],
          selected: status,
        };
      }
    }
    return arr;
  };

  const isItemSelected = (e) => {
    setSelect(e.target.checked);

    const changedProdList = selectedProd(prodList, id, e.target.checked);

    dispatch(selectProdAction([...changedProdList]));
  };

  useEffect(() => {
    for (let i = 0; i < prodList.length; i++) {
      if (prodList[i].id === id && prodList[i].selected) {
        setSelect(true);
      } else if (prodList[i].id === id && !prodList[i].selected) {
        setSelect(false);
      }
    }
  }, [prodList, id]);

  const addOnClick = () => {
    Auth.checkTokenValidity();
    Cart.addItemToCart(id).then(() => {
      setOpen(true);
    });
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const deleteProd = () => {
    Admin.deleteProduct(id).then(() => {
      Products.getProducts().then((item) => {
        dispatch(addProductsAction(item));
      });
    });
  };

  return (
    <>
      <div
        className={
          "catalog__selector" + [select ? " catalog__selector-selected" : ""]
        }
      >
        <Checkbox
          color="primary"
          size="medium"
          onChange={isItemSelected}
          checked={select}
        />
        <div className="prod__events">
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={addOnClick}
          >
            Add To Cart
          </Button>
          {localStorage.getItem("user") &&
            JSON.parse(localStorage.getItem("user")).data.isAdmin && (
              <>
                <DeleteIcon
                  color="primary"
                  fontSize="small"
                  onClick={deleteProd}
                />
                <EditIcon
                  color="primary"
                  fontSize="small"
                  onClick={() => dispatch(changeEditProdModalId(id))}
                />
              </>
            )}
        </div>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="success">
          Added Successfully!
        </Alert>
      </Snackbar>
    </>
  );
};
export default CatalogProductSelector;
