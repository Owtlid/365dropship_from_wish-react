import "./EditProd.css";
import {
  Dialog,
  DialogTitle,
  TextField,
  Snackbar,
  Button,
} from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import { Admin, Products } from "../../API/Request";
import MuiAlert from "@material-ui/lab/Alert";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addProductsAction,
  changeEditProdModalId,
} from "../../Store/Products/ProductsActions";

const productValidation = yup.object().shape({
  id: yup.number().integer().required(),
  title: yup.string().min(2).max(50).required(),
  description: yup.string().min(10).max(580).required(),
  price: yup.number().min(1).required(),
  imageUrl: yup.string().url().required(),
});

const EditProd = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [modalStatus, setModalStatus] = useState(false);
  const prodId = useSelector((state) => state.products.editProdModalId);
  const [recievedVals, setRecievedVals] = useState({});

  const handleSubmit = (values) => {
    Admin.updateProduct(prodId, values).then(() => {
      setOpen(true);
      Products.getProducts().then((item) => {
        dispatch(addProductsAction(item));
      });
      Products.getSingleProduct(prodId).then((res) => {
        setRecievedVals(res);
      });
    });
  };

  useEffect(() => {
    if (prodId) {
      Products.getSingleProduct(prodId).then((res) => {
        setRecievedVals(res);
        setModalStatus(true);
      });
    }
  }, [prodId]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const customIdTextField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type="text"
        error={touched.id && errors.id ? true : false}
        label="id"
        name="id"
        disabled
        variant="outlined"
        {...field}
        {...props}
      />
    );
  };

  const customTitleTextField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type="text"
        error={touched.title && errors.title ? true : false}
        label="Title"
        name="title"
        variant="outlined"
        {...field}
        {...props}
      />
    );
  };

  const customDescriptionTextField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type="text"
        error={touched.description && errors.description ? true : false}
        label="Description"
        name="description"
        variant="outlined"
        fullWidth
        multiline
        rows={12}
        {...field}
        {...props}
      />
    );
  };

  const customPriceTextField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type="text"
        error={touched.price && errors.price ? true : false}
        label="Price"
        name="price"
        variant="outlined"
        {...field}
        {...props}
      />
    );
  };
  const customImageTextField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type="text"
        error={touched.imageUrl && errors.imageUrl ? true : false}
        label="ImageUrl"
        name="imageUrl"
        variant="outlined"
        {...field}
        {...props}
      />
    );
  };

  const ccl = () => {
    setModalStatus(false);
    dispatch(changeEditProdModalId(null));
  };

  return (
    <>
      <Dialog
        onClose={ccl}
        aria-labelledby="customized-dialog-title"
        open={modalStatus}
        fullWidth
        maxWidth='lg'
      >
        <div className='container'>
        <div className="edit__prods">
          <DialogTitle onClose={ccl}>Edit Product</DialogTitle>
          <Formik
            enableReinitialize
            initialValues={{
              id: recievedVals ? recievedVals.id : null,
              title: recievedVals ? recievedVals.title : "",
              description: recievedVals ? recievedVals.description : "",
              price: recievedVals ? recievedVals.price : "",
              imageUrl: recievedVals ? recievedVals.imageUrl : "",
            }}
            onSubmit={handleSubmit}
            validationSchema={productValidation}
          >
            <Form className="form">
              <Field name="id" component={customIdTextField} />
              <Field name="title" component={customTitleTextField} />
              <Field
                name="description"
                component={customDescriptionTextField}
              />
              <Field name="price" component={customPriceTextField} />
              <Field name="imageUrl" component={customImageTextField} />
              <Button type="submit" color="primary" variant="contained">
                Edit
              </Button>
            </Form>
          </Formik>
        </div>
        <div className="modal">
        <div className="modal__photo">
          <div className="photo__price">
            <ul className="photo__price-list">
              <li>{recievedVals.price}$</li>
              <li>{recievedVals.price}$</li>
              <li>{recievedVals.price}$</li>
            </ul>
          </div>
          <div className="photo__slideshow">
            <div className="slideshow__primary">
              <img src={recievedVals.imageUrl} alt="productImage" />
            </div>
            <ul className="slideshow__secondary">
              <li>
                <img src={recievedVals.imageUrl} alt="productImage" />
              </li>
              <li>
                <img src={recievedVals.imageUrl} alt="productImage" />
              </li>
              <li>
                <img src={recievedVals.imageUrl} alt="productImage" />
              </li>
            </ul>
          </div>
        </div>
        <div className="modal__title">
          <DialogTitle onClose={handleClose}>{recievedVals.title}</DialogTitle>
          <div className="title__tabs">
            <div className="title__tabs-item"> Product Details </div>
            <div className="title__tabs-item"> Shipping Rates </div>
            <div className="title__tabs-item"> Available Options </div>
          </div>
          <div className="title__desc">
            <p>{recievedVals.description}</p>
          </div>
        </div>
      </div>
      </div>
      </Dialog>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="success">
          Edited Successfully!
        </Alert>
      </Snackbar>
    </>
  );
};
export default EditProd;
