import "./AddProd.css";
import {
  Dialog,
  DialogTitle,
  TextField,
  Snackbar,
  Button,
} from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import { Admin, Products } from "../../API/Request";
import MuiAlert from "@material-ui/lab/Alert";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addProductsAction,
  changeAddProdModalStatus,
} from "../../Store/Products/ProductsActions";

const productValidation = yup.object().shape({
  title: yup.string().min(2).max(50).required(),
  description: yup.string().min(10).max(580).required(),
  price: yup.number().min(1).required(),
  imageUrl: yup.string().url().required(),
});

const AddProd = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const modalStatus = useSelector((state) => state.products.addProdModalStatus);

  const handleSubmit = (values) => {
    Admin.addProduct(values).then(() => {
      setOpen(true);
      Products.getProducts().then((item) => {
        dispatch(addProductsAction(item));
      });
    });
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const customTitleTextField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type="text"
        error={touched.title && errors.title ? true : false}
        label="Title"
        name="title"
        variant="outlined"
        {...field}
        {...props}
      />
    );
  };

  const customDescriptionTextField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type="text"
        error={touched.description && errors.description ? true : false}
        label="Description"
        name="description"
        variant="outlined"
        multiline
        rows={12}   

        
        fullWidth
        {...field}
        {...props}
      />
    );
  };

  const customPriceTextField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type="text"
        error={touched.price && errors.price ? true : false}
        label="Price"
        name="price"
        variant="outlined"
        {...field}
        {...props}
      />
    );
  };
  const customImageTextField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type="text"
        error={touched.imageUrl && errors.imageUrl ? true : false}
        label="ImageUrl"
        name="imageUrl"
        variant="outlined"
        {...field}
        {...props}
      />
    );
  };

  return (
    <>
      <Dialog
        onClose={() => dispatch(changeAddProdModalStatus(false))}
        aria-labelledby="customized-dialog-title"
        open={modalStatus}
        fullWidth
        maxWidth="xs"
      >
        <div className="add__prods">
          <DialogTitle
            onClose={() => dispatch(changeAddProdModalStatus(false))}
          >
            Add Product
          </DialogTitle>
          <Formik
            enableReinitialize
            initialValues={{
              title: "",
              description: "",
              price: "",
              imageUrl: "",
            }}
            onSubmit={handleSubmit}
            validationSchema={productValidation}
          >
            <Form className="add__form">
              <Field name="title" component={customTitleTextField} />
              <Field
                name="description"
                component={customDescriptionTextField}
              />
              <Field name="price" component={customPriceTextField} />
              <Field name="imageUrl" component={customImageTextField} />
              <Button type="submit" color="primary" variant="contained">
                Add
              </Button>
            </Form>
          </Formik>
        </div>
      </Dialog>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="success">
          Added Successfully!
        </Alert>
      </Snackbar>
    </>
  );
};
export default AddProd;
