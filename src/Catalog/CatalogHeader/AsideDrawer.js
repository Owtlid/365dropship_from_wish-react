import HomeIcon from "@material-ui/icons/Home";
import DropshipLogo from "../../Aside/DropshipLogo";
import { Link } from "react-router-dom";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import SpeedIcon from "@material-ui/icons/Speed";
import MarkunreadMailboxIcon from "@material-ui/icons/MarkunreadMailbox";
import AssignmentIcon from "@material-ui/icons/Assignment";
import SwapHorizontalCircleIcon from "@material-ui/icons/SwapHorizontalCircle";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import { Drawer } from "@material-ui/core";
import { useState } from "react";
import useStyles from "./AsideDrawer.styles";

const AsideDrawer = () => {
  const classess = useStyles();

  const [homeStatus, setHomeStatus] = useState(false);
  return (
    <>
      <HomeIcon
        fontSize="large"
        color="primary"
        className={classess.root}
        onClick={() => setHomeStatus(true)}
      />
      <Drawer
        open={homeStatus}
        onClose={() => setHomeStatus(false)}
        anchor="left"
      >
        <DropshipLogo />
        <div className="aside__nav">
          <Link to="/user">
            <AccountCircleIcon fontSize="large" color="secondary" />
          </Link>
          <Link to="/dashboard">
            <SpeedIcon fontSize="default" color="secondary" />
          </Link>
          <Link to="/catalog">
            <FormatListBulletedIcon fontSize="default" color="secondary" />
          </Link>
          <MarkunreadMailboxIcon fontSize="default" color="secondary" />
          <Link to="/cart">
            <ShoppingCartIcon fontSize="default" color="secondary" />
          </Link>
          <AssignmentTurnedInIcon fontSize="default" color="secondary" />
          <SwapHorizontalCircleIcon fontSize="default" color="secondary" />
          <AssignmentIcon fontSize="default" color="secondary" />
        </div>
      </Drawer>
    </>
  );
};

export default AsideDrawer;
