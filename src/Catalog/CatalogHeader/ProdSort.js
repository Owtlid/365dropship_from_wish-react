const sortProd = (arr, type) => {
  type === "LTH" && arr.sort((a, b) => a.price - b.price);
  type === "HTL" && arr.sort((a, b) => b.price - a.price);
  type === "ATZ" && arr.sort((a, b) => a.title.localeCompare(b.title));
  type === "ZTA" && arr.sort((a, b) => b.title.localeCompare(a.title));
  type === "" && arr.sort((a, b) => a.id - b.id);
  return arr;
};
export default sortProd;
