import "./ProductsQuantity.css";
import { Button } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { selectAllAction } from "../../Store/Products/ProductsActions";
const ProductsQuantity = () => {
  const dispatch = useDispatch();
  const quantity = useSelector(
    (state) => state.products.productsForDisplay.length
  );
  const prodList = useSelector((state) => state.products.productsForDisplay);
  const [selectedQTY, setSelectedQTY] = useState(0);

  useEffect(() => {
    let qty = 0;
    for (let i = 0; i < prodList.length; i++) {
      if (prodList[i].selected) {
        qty += 1;
      }
    }
    setSelectedQTY(qty);
  }, [prodList]);

  const selectAll = (arr, status) => {
    for (let i = 0; i < arr.length; i++) {
      arr[i] = {
        ...arr[i],
        selected: status,
      };
    }
    return arr;
  };

  const buttonClick = (e) => {
    if (e.target.innerText === "SELECT ALL") {
      const selectedArr = selectAll(prodList, true);
      dispatch(selectAllAction([...selectedArr]));
    }
    if (e.target.innerText === "CLEAR ALL") {
      const unSelectedArr = selectAll(prodList, false);
      dispatch(selectAllAction([...unSelectedArr]));
    }
  };

  return (
    <div className="header__select">
      <Button variant="contained" color="primary" onClick={buttonClick}>
        {selectedQTY === 0 ? "SELECT ALL" : "CLEAR ALL"}
      </Button>
      <span className="header__counter">
        selected {selectedQTY} out of {quantity} products
      </span>
    </div>
  );
};
export default ProductsQuantity;
