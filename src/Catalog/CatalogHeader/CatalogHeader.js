import "./CatalogHeader.css";
import ProductsQuantity from "./ProductsQuantity";
import SearchInput from "./SearchInput";
import HeaderAbout from "./HeaderAbout";
import HeaderSort from "./HeaderSort";
import { Drawer, Button } from "@material-ui/core";
import Niche from "../CatalogFilter/Niche";
import Category from "../CatalogFilter/Category";
import Selector from "../CatalogFilter/Selector";
import Sliderzz from "../CatalogFilter/Sliderzz";
import { useState } from "react";
import MenuIcon from "@material-ui/icons/Menu";
import AsideDrawer from "./AsideDrawer";
import useStyles from "./CatalogHeader.styles";
import { useDispatch } from "react-redux";
import { changeAddProdModalStatus } from "../../Store/Products/ProductsActions";

const CatalogHeader = () => {
  const classess = useStyles();
  const dispatch = useDispatch();

  const [drawerStatus, setDrawerStatus] = useState(false);

  return (
    <div className="catalog__header">
      <div className="catalog__header-firstRow">
        <ProductsQuantity />
        <div className="header__searchAbout">
          <AsideDrawer />
          {localStorage.getItem("user") &&
            JSON.parse(localStorage.getItem("user")).data.isAdmin && (
              <Button
                color="primary"
                variant="contained"
                onClick={() => dispatch(changeAddProdModalStatus(true))}
              >
                Add Product
              </Button>
            )}

          <SearchInput />
          <HeaderAbout />
          <MenuIcon
            fontSize="large"
            color="primary"
            className={classess.root}
            onClick={() => setDrawerStatus(true)}
          />
          <Drawer
            open={drawerStatus}
            onClose={() => setDrawerStatus(false)}
            anchor="right"
          >
            <div className="drawer">
              <Niche />
              <Category />
              <Selector name="Ship From" />
              <Selector name="Ship To" />
              <Sliderzz name="PRICE RANGE" />
            </div>
          </Drawer>
        </div>
      </div>
      <div className="catalog__header-scndRow">
        <HeaderSort />
      </div>
    </div>
  );
};
export default CatalogHeader;
