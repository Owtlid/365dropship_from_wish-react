import "./HeaderAbout.css";
import HelpOutlineOutlinedIcon from "@material-ui/icons/HelpOutlineOutlined";
import { Button } from "@material-ui/core";
import { useSelector } from "react-redux";
import { Cart } from "../../API/Request";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { useState } from "react";

const HeaderInv = () => {
  const prodList = useSelector((state) => state.products.productsForDisplay);
  const [open, setOpen] = useState(false);

  const addMultipleItemsToCart = async () => {
    for (let i = 0; i < prodList.length; i++) {
      if (prodList[i].selected) {
        await Cart.addItemToCart(prodList[i].id).then(() => {
          setOpen(true);
        });
      }
    }
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  return (
    <div className="header__inventory">
      <Button
        variant="contained"
        color="primary"
        onClick={addMultipleItemsToCart}
      >
        Add To Cart
      </Button>
      <HelpOutlineOutlinedIcon fontSize="large" color="secondary" />
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="success">
          Cleared Successfully!
        </Alert>
      </Snackbar>
    </div>
  );
};
export default HeaderInv;
