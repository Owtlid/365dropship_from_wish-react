import "./HeaderSort.css";
import SortIcon from "@material-ui/icons/Sort";
import { useDispatch, useSelector } from "react-redux";
import {
  changeSortedProductAction,
  changeSortTypeAction,
} from "../../Store/Products/ProductsActions";
import sortProd from "./ProdSort";
import { useEffect } from "react";

const HeaderSort = () => {
  const dispatch = useDispatch();
  const prodList = useSelector((state) => state.products.productsForDisplay);
  const sortType = useSelector((state) => state.products.sortType);

  const sortFunc = (e) => {
    dispatch(changeSortTypeAction(e.target.value));
  };

  useEffect(() => {
    const sortedList = sortProd(prodList, sortType);
    dispatch(changeSortedProductAction(sortedList));
  }, [sortType, prodList, dispatch]);

  return (
    <div className="header__sort">
      <div className="header__sort-label">
        <SortIcon fontSize="large" />
        <span>SortBy:</span>
      </div>
      <select onChange={sortFunc}>
        <option value=""></option>
        <option value="LTH">Low to High(price)</option>
        <option value="HTL">High to Low(price)</option>
        <option value="ATZ">A to Z</option>
        <option value="ZTA">Z to A</option>
      </select>
    </div>
  );
};
export default HeaderSort;
