import "./SearchInput.css";
import { useEffect } from "react";
import { TextField } from "@material-ui/core";
import { Products } from "../../API/Request";
import { useDispatch, useSelector } from "react-redux";
import {
  initialiseProductsForLogicAction,
  searchProdAction,
  searchQueryAction,
} from "../../Store/Products/ProductsActions";
import sortProd from "./ProdSort";

const SearchInput = () => {
  const dispatch = useDispatch();

  const sortType = useSelector((state) => state.products.sortType);
  const allProds = useSelector((state) => state.products.productsForLogic);
  const query = useSelector((state) => state.products.searchQuery);

  useEffect(() => {
    Products.getProducts().then((item) => {
      dispatch(initialiseProductsForLogicAction(item));
    });
  }, [dispatch]);

  const ChangeQuery = (e) => {
    dispatch(searchQueryAction(e.target.value));
  };

  useEffect(() => {
    const searchInput = setTimeout(() => {
      const filteredArr = allProds.filter((item) =>
        item.title.toLowerCase().includes(query.toLowerCase())
      );
      const sortedProds = sortProd(filteredArr, sortType);
      dispatch(searchProdAction(sortedProds));
    }, 800);
    return () => {
      clearTimeout(searchInput);
    };

    // eslint-disable-next-line
  }, [query, allProds, dispatch]);

  return (
    <div className="header__search">
      <TextField
        id="outlined-search"
        label="Search field"
        type="search"
        variant="outlined"
        size="small"
        value={query}
        onChange={ChangeQuery}
      />
    </div>
  );
};
export default SearchInput;
