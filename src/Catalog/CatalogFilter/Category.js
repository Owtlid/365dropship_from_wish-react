import "./Category.css";
import { useState } from "react";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";

const Category = () => {
  const [showCategoryList, setShowCategoryList] = useState(false);

  return (
    <>
      <div
        className="filter__category"
        onClick={() => setShowCategoryList((prevVal) => !prevVal)}
      >
        Choose Category
        {!showCategoryList ? (
          <KeyboardArrowDownIcon fontSize="default" />
        ) : (
          <KeyboardArrowUpIcon fontSize="default" />
        )}
      </div>
      {showCategoryList && (
        <ul className="filter__category-list">
          <li>...</li>
        </ul>
      )}
    </>
  );
};

export default Category;
