import "./Filter.css";
import Niche from "./Niche";
import Category from "./Category";
import Selector from "./Selector";
import Sliderzz from "./Sliderzz";

const Filter = () => {
  return (
    <div className="catalog__filter">
      <Niche />
      <Category />
      <div className="filter__selectors">
        <Selector name="Ship From" />
        <Selector name="Ship To" />
        <Selector name="select supplier" />
      </div>
      <Sliderzz name="PRICE RANGE" />
    </div>
  );
};

export default Filter;
