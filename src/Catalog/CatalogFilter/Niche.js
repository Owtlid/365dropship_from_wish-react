import "./Niche.css";
import { useState } from "react";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";

const Niche = () => {
  const [showNicheList, setShowNicheList] = useState(false);

  return (
    <>
      <div
        className="filter__niche"
        onClick={() => setShowNicheList((prevVal) => !prevVal)}
      >
        Choose Niche{" "}
        {!showNicheList ? (
          <KeyboardArrowDownIcon fontSize="default" />
        ) : (
          <KeyboardArrowUpIcon fontSize="default" />
        )}
      </div>
      {showNicheList && (
        <ul className="filter__niche-list">
          <li>...</li>
        </ul>
      )}
    </>
  );
};

export default Niche;
