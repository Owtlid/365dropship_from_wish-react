import "./Sliderzz.css";
import { Slider } from "@material-ui/core";
import { useEffect, useState } from "react";
import useStyles from "./Sliderzz.styles";
import { useDispatch, useSelector } from "react-redux";
import { Products } from "../../API/Request";
import sortProd from "../CatalogHeader/ProdSort";
import {
  addProductsAction,
  changeFilterValuesAction,
  initialiseProductsForLogicAction,
} from "../../Store/Products/ProductsActions";
import { Button } from "@material-ui/core";

const Sliderzz = ({ name }) => {
  const dispatch = useDispatch();
  const [value, setValue] = useState([]);
  const sortType = useSelector((state) => state.products.sortType);

  const allProds = useSelector((state) => state.products.productsForLogic);
  const maxValue = useSelector((state) => state.products.filterValues[1]);
  const minValue = useSelector((state) => state.products.filterValues[0]);

  useEffect(() => {
    Products.getProducts().then((item) => {
      dispatch(initialiseProductsForLogicAction(item));
      let maxPrice = item[0].price;
      let minPrice = item[0].price;
      for (let i = 0; i < item.length; i++) {
        if (item[i].price > maxPrice) {
          maxPrice = item[i].price;
        }
        if (item[i].price < minPrice) {
          minPrice = item[i].price;
        }
      }
      dispatch(changeFilterValuesAction([minPrice, maxPrice]));
      setValue([minPrice, maxPrice]);
    });
  }, [dispatch]);

  useEffect(() => {
    const filteredArr = allProds.filter(
      (item) => item.price >= value[0] && item.price <= value[1]
    );
    const sortedProds = sortProd(filteredArr, sortType);
    dispatch(addProductsAction(sortedProds));
  }, [value, dispatch, allProds, sortType]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const resetFilter = () => {
    setValue([minValue, maxValue]);
  };

  const classes = useStyles();

  return (
    <div className="filter__slider">
      <span>{name}</span>
      <Slider
        className={classes.root}
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        max={maxValue}
      />
      <Button variant="contained" color="primary" onClick={resetFilter}>
        RESET FILTER
      </Button>
    </div>
  );
};

export default Sliderzz;
