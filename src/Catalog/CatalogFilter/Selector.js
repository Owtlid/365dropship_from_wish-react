import "./Selector.css";
import { useState } from "react";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";

const Selector = ({ name }) => {
  const [showCountriesList, setShowCountriesList] = useState(false);

  return (
    <div
      className="filter__selector"
      onClick={() => {
        setShowCountriesList((prevVal) => !prevVal);
      }}
    >
      <div className="selector__header">
        <span>{name}</span>
        {!showCountriesList ? (
          <KeyboardArrowDownIcon fontSize="small" color="secondary" />
        ) : (
          <KeyboardArrowUpIcon fontSize="small" color="secondary" />
        )}
      </div>
    </div>
  );
};
export default Selector;
