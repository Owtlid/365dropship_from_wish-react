import "./Dashboard.css";
import Header from "./Header";
import Welcome from "./Welcome";
import Info from "./Info";
import Map from "./Map";
const Dashboard = () => {
  return (
    <div className="dashboard">
      <Header />
      <Welcome />
      <Info />
      <Map />
    </div>
  );
};
export default Dashboard;
