import "./Map.css";
import map from "../Assets/map.PNG";

const Map = () => {
  return (
    <div className="dashboard__map">
      <img src={map} alt="map"></img>
    </div>
  );
};
export default Map;
