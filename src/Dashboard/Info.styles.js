import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  table: {
    minWidth: 200,
    minHeight: 250,
    boxSizing: "border-box",
  },
  header: {
    width: 150,
    display: "flex",
    paddingLeft: 24,
    alignItems: "center",
    whiteSpace: "nowrap",
  },
});

export default useStyles;
