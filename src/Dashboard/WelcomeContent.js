import { Button } from "@material-ui/core";

const WelcomeContent = ({ heading, img, text, btn }) => {
  return (
    <div className="content__box">
      <img src={img} alt="wizard1" />
      <h3> {heading} </h3>
      <span>{text}</span>
      {btn && (
        <div className="content__box-Buttons">
          <Button variant="contained" color="primary">
            Start
          </Button>
          <Button variant="outlined">Skip</Button>
        </div>
      )}
    </div>
  );
};
export default WelcomeContent;
