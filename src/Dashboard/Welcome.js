import "./Welcome.css";
import wizard1 from "../Assets/wizard1.svg";
import wizard2 from "../Assets/wizard2.svg";
import wizard3 from "../Assets/wizard3.svg";
import wizard4 from "../Assets/wizard4.svg";
import WelcomeContent from "./WelcomeContent";

const Welcome = () => {
  return (
    <div className="dashboard__welcome">
      <div className="welcome__header">
        <h2>Welcome, you’re almost ready to go!! </h2>
        <span>Follow these steps to get started</span>
      </div>
      <div className="welcome__content">
        <WelcomeContent
          heading="1. Complete your profile details"
          img={wizard1}
          text="Finalize your account set-up, choose your plan and fill in your payment method"
          btn
        />
        <WelcomeContent
          heading="2. Explore catalog and add items to your inventory"
          img={wizard2}
          text="Fill in your inventory, easily filter and search to find the products most suitable for you"
        />
        <WelcomeContent
          heading="3. Add your eCommerce store"
          img={wizard3}
          text="Integrate one or more eCommerce stores to your account"
          btn
        />
        <WelcomeContent
          heading="Synchronize/Export your inventory"
          img={wizard4}
          text="Synchronize the products in your inventory to your eCommerce store and start generating sales"
        />
      </div>
    </div>
  );
};

export default Welcome;
