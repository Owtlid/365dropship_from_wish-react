import "./Info.css";
import bestSeller from "../Assets/bestSeller.png";
import {
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Paper,
} from "@material-ui/core";
import it from "../Assets/it.svg";
import gb from "../Assets/gb.svg";
import de from "../Assets/de.svg";
import us from "../Assets/us.svg";
import useStyles from "./Info.styles";

const Info = () => {
  const classes = useStyles();

  return (
    <div className="dashboard__info">
      <div className="info__bestSeller">
        <img src={bestSeller} alt="bestSellers" />
      </div>
      <div className="info__table">
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>
                  <h2> countries </h2>
                </TableCell>
                <TableCell align="right">
                  <h2>suppliers</h2>
                </TableCell>
                <TableCell align="right">
                  <h2>products</h2>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell component="th">
                  <div className="cntr">
                    <img src={us} alt="USFlag" />
                    <span>United States</span>
                  </div>
                </TableCell>
                <TableCell align="right">
                  <span>21</span>
                </TableCell>
                <TableCell align="right">
                  <span>52,987</span>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th">
                  <div className="cntr">
                    <img src={gb} alt="GBFlag" />
                    <span>United Kingdom</span>
                  </div>
                </TableCell>
                <TableCell align="right">
                  <span>8</span>
                </TableCell>
                <TableCell align="right">
                  <span>14,826</span>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th">
                  <div className="cntr">
                    <img src={de} alt="GermanyFlag" />
                    <span>Germany</span>
                  </div>
                </TableCell>
                <TableCell align="right">
                  <span>4</span>
                </TableCell>
                <TableCell align="right">
                  <span>5,517</span>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th">
                  <div className="cntr">
                    <img src={it} alt="ItalyFlag" />
                    <span>Italy</span>
                  </div>
                </TableCell>
                <TableCell align="right">
                  <span>4</span>
                </TableCell>
                <TableCell align="right">
                  <span>17,423</span>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
};
export default Info;
