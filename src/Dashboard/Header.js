import "./Header.css";
import HelpOutlineOutlinedIcon from "@material-ui/icons/HelpOutlineOutlined";
import AsideDrawer from "../Catalog/CatalogHeader/AsideDrawer";

const Header = () => {
  return (
    <div className="dashboard__header">
      <AsideDrawer />
      <span>DASHDOARD</span>
      <div className="header__plan">
        <div className="plan__status">
          <span>Current Plan</span>
          <span>Free</span>
        </div>
        <div className="about">
          <HelpOutlineOutlinedIcon fontSize="large" color="secondary" />
        </div>
      </div>
    </div>
  );
};
export default Header;
