import "./App.css";
import { Switch, Route } from "react-router-dom";
import LandingPage from "./LandingPage/LandingPage";
import Aside from "./Aside/Aside";
import Catalog from "./Catalog/Catalog";
import Dashboard from "./Dashboard/Dashboard";
import Registration from "./Authorization/Registration";
import Login from "./Authorization/Login";
import User from "./User/User";
import Cart from "./Cart/Cart";
import { useEffect, useState } from "react";
import { Auth } from "./API/Request";

function App() {
  const [isAuth, setIsAuth] = useState(false);

  useEffect(() => {
    if (localStorage.getItem("user") && localStorage.getItem("token")) {
      Auth.checkTokenValidity();
      setIsAuth(true);
    } else {
      setIsAuth(false);
    }
  }, [setIsAuth]);

  return (
    <div className="App">
      <Aside />
      {isAuth ? (
        <Switch>
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/user" component={User} />
          <Route path="/catalog/:slug?" component={Catalog} />
          <Route path="/cart" component={Cart} />
          <Route path="/" component={LandingPage} />
        </Switch>
      ) : (
        <Switch>
          <Route path="/registration" component={Registration} />
          <Route path="/login" component={Login} />
          <Route path="/" component={LandingPage} />
        </Switch>
      )}
    </div>
  );
}

export default App;
