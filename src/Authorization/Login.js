import "./Combined.css";
import logo from "../Assets/logo.svg";
import { Button, TextField, InputAdornment } from "@material-ui/core";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import VpnKeyOutlinedIcon from "@material-ui/icons/VpnKeyOutlined";
import FacebookIcon from "@material-ui/icons/Facebook";
import GitHubIcon from "@material-ui/icons/GitHub";
import { Link } from "react-router-dom";
import { useState } from "react";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { Auth } from "../API/Request";
import { Field, Form, Formik } from "formik";
import { logInSchema } from "./ValidationSchemes";

const Login = () => {
  const [showPassword, setShowPassword] = useState(false);

  const signInFunc = (values) => {
    Auth.logIn(values.email, values.password);
  };

  const customEmailInput = ({ field, form: { touched, errors }, ...props }) => {
    return (
      <TextField
        type="email"
        name="email"
        label="Email"
        variant="outlined"
        error={touched.email && errors.email ? true : false}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <MailOutlineIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customPasswordInput = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        type={!showPassword ? "password" : "text"}
        error={touched.password && errors.password ? true : false}
        label="password"
        name="password"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <VpnKeyOutlinedIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  return (
    <div className="authorization">
      <div className="authorization__login">
        <div className="login__header">
          <img src={logo} alt="logo" />
          <h3>Members Log In</h3>
        </div>
        <Formik
          enableReinitialize
          initialValues={{
            email: "",
            password: "",
          }}
          onSubmit={signInFunc}
          validationSchema={logInSchema}
        >
          <Form className="login__inputs">
            <div className="inputs__email">
              <Field name="email" component={customEmailInput} />
            </div>
            <div className="password__logIn">
              <Field name="password" component={customPasswordInput} />
              {!showPassword ? (
                <VisibilityIcon
                  fontSize="small"
                  color="primary"
                  onClick={() => setShowPassword((prevVal) => !prevVal)}
                />
              ) : (
                <VisibilityOffIcon
                  fontSize="small"
                  color="primary"
                  onClick={() => setShowPassword((prevVal) => !prevVal)}
                />
              )}
            </div>
            <span>
              <a href="https://app.365dropship.com/user/pages/forgot-password">
                Forgot password?
              </a>
            </span>
            <Button variant="contained" color="primary" fullWidth type="submit">
              Sign In
            </Button>
          </Form>
        </Formik>
        <div className="login__login">
          <Button variant="outlined" size="small">
            <GitHubIcon fontSize="small" color="primary" />
          </Button>
          <Button variant="outlined" size="small">
            <FacebookIcon fontSize="small" color="primary" />
          </Button>
        </div>
        <div className="login__redirect">
          <span>
            Don't have an account? <Link to="/registration">Sign Up</Link>
          </span>
        </div>
      </div>
    </div>
  );
};

export default Login;
