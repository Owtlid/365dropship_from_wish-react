import * as yup from "yup";
const registrationSchema = yup.object().shape({
  firstName: yup.string().required().min(2, "too short").max(20, "too long"),
  lastName: yup.string().required().min(2, "too short").max(20, "too long"),
  email: yup.string().email().required(),
  password: yup.string().required().min(4, "too short"),
  passwordConfirmation: yup.string().required().min(4, "too short"),
});
const logInSchema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required().min(4, "password too short"),
});
export { logInSchema, registrationSchema };
