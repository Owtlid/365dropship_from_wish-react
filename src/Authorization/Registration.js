import "./Combined.css";
import { Button, Checkbox, TextField, InputAdornment } from "@material-ui/core";
import logo from "../Assets/logo.svg";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import VpnKeyOutlinedIcon from "@material-ui/icons/VpnKeyOutlined";
import BorderColorOutlinedIcon from "@material-ui/icons/BorderColorOutlined";
import FacebookIcon from "@material-ui/icons/Facebook";
import GitHubIcon from "@material-ui/icons/GitHub";
import { Link } from "react-router-dom";
import { useState } from "react";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { Auth } from "../API/Request";
import { Field, Form, Formik } from "formik";
import { registrationSchema } from "./ValidationSchemes";

const Registration = () => {
  const [showPassword, setShowPassword] = useState(false);

  const signUpFunc = (values) => {
    Auth.registration(values);
  };

  const customFirstNameField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.firstName && errors.firstName ? true : false}
        type="text"
        name="firstName"
        label="First Name"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <BorderColorOutlinedIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customLastNameField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.lastName && errors.lastName ? true : false}
        type="text"
        name="lastName"
        label="Last Name"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <BorderColorOutlinedIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customEmailInput = ({ field, form: { touched, errors }, ...props }) => {
    return (
      <TextField
        error={touched.email && errors.email ? true : false}
        type="email"
        name="email"
        label="Email"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <MailOutlineIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customPasswordInput = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.password && errors.password ? true : false}
        type={!showPassword ? "password" : "text"}
        name="password"
        label="Password"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <VpnKeyOutlinedIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };
  const customConfirmPasswordInput = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={
          touched.passwordConfirmation && errors.passwordConfirmation
            ? true
            : false
        }
        type={!showPassword ? "password" : "text"}
        name="passwordConfirmation"
        label="Password Confirmation"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <VpnKeyOutlinedIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  return (
    <div className="authorization">
      <div className="authorization__registration">
        <div className="registration__header">
          <img src={logo} alt="logo" />
          <h3>Sign Up</h3>
        </div>
        <div className="registration__inputs">
          <Formik
            enableReinitialize
            initialValues={{
              firstName: "",
              lastName: "",
              email: "",
              password: "",
            }}
            onSubmit={signUpFunc}
            validationSchema={registrationSchema}
          >
            <Form className="inputs__form">
              <div className="single__input">
                <Field name="firstName" component={customFirstNameField} />
              </div>
              <div className="single__input">
                <Field name="lastName" component={customLastNameField} />
              </div>
              <div className="single__input">
                <Field name="email" component={customEmailInput} />
              </div>
              <div className="password">
                <Field name="password" component={customPasswordInput} />

                {!showPassword ? (
                  <VisibilityIcon
                    fontSize="small"
                    color="primary"
                    onClick={() => setShowPassword((prevVal) => !prevVal)}
                  />
                ) : (
                  <VisibilityOffIcon
                    fontSize="small"
                    color="primary"
                    onClick={() => setShowPassword((prevVal) => !prevVal)}
                  />
                )}
              </div>
              <div className="password">
                <Field
                  name="passwordConfirmation"
                  component={customConfirmPasswordInput}
                />

                {!showPassword ? (
                  <VisibilityIcon
                    fontSize="small"
                    color="primary"
                    onClick={() => setShowPassword((prevVal) => !prevVal)}
                  />
                ) : (
                  <VisibilityOffIcon
                    fontSize="small"
                    color="primary"
                    onClick={() => setShowPassword((prevVal) => !prevVal)}
                  />
                )}
              </div>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
              >
                Sign Up
              </Button>
            </Form>
          </Formik>
          <span>
            By creating an account, you agree with the{" "}
            <a href="https://www.365dropship.com/terms-of-service/">
              Terms & Conditions
            </a>{" "}
            and{" "}
            <a href="https://www.365dropship.com/terms-of-service/">
              Privacy Policy
            </a>
          </span>
          <div className="inputs__subscribe">
            <Checkbox size="small" color="primary" />
            <span>Subscribe to Newsletter</span>
          </div>
          <div className="registration__login">
            <Button variant="outlined" size="small">
              <GitHubIcon fontSize="small" color="primary" />
            </Button>
            <Button variant="outlined" size="small">
              <FacebookIcon fontSize="small" color="primary" />
            </Button>
          </div>
          <div className="registration__redirect">
            <span>
              Already have an account? <Link to="/login">Sign in</Link>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Registration;
