import "./Header.css";
import HelpOutlineOutlinedIcon from "@material-ui/icons/HelpOutlineOutlined";
import { useHistory } from "react-router-dom";
import AsideDrawer from "../Catalog/CatalogHeader/AsideDrawer";

const Header = () => {
  const history = useHistory();

  const SignOut = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    history.go("/");
  };

  return (
    <div className="user__header">
      <AsideDrawer />
      <span>My Profile</span>
      <div className="header__btn">
        <div className="btn__signout" onClick={SignOut}>
          SIGN OUT
        </div>
        <HelpOutlineOutlinedIcon fontSize="large" color="secondary" />
      </div>
    </div>
  );
};
export default Header;
