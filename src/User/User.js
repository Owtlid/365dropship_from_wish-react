import "./User.css";
import Header from "./Header";
import UserInfo from "./UserInfo";
const User = () => {
  return (
    <div className="user">
      <Header />
      <UserInfo />
    </div>
  );
};
export default User;
