import "./UserInfo.css";
import pht from "../Assets/profile-example.jpg";
import { Button, TextField, InputAdornment } from "@material-ui/core";
import { userDetailsScheam } from "./UserDetailsValidation";
import BorderColorOutlinedIcon from "@material-ui/icons/BorderColorOutlined";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import VpnKeyOutlinedIcon from "@material-ui/icons/VpnKeyOutlined";
import { Field, Form, Formik } from "formik";
import { Auth, User } from "../API/Request";
import { useEffect, useState } from "react";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";

const UserInfo = () => {
  const customFirstNameField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.firstName && errors.firstName ? true : false}
        type="text"
        name="firstName"
        label="First Name"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <BorderColorOutlinedIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customLastNameField = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.lastName && errors.lastName ? true : false}
        type="text"
        name="lastName"
        label="Last Name"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <BorderColorOutlinedIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customEmailInput = ({ field, form: { touched, errors }, ...props }) => {
    return (
      <TextField
        error={touched.email && errors.email ? true : false}
        type="email"
        name="email"
        label="Email"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <MailOutlineIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customPasswordInput = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.password && errors.password ? true : false}
        type="text"
        name="password"
        label="Password"
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <VpnKeyOutlinedIcon fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const [recievedVals, setRecievedVals] = useState({});

  useEffect(() => {
    User.getSingleUser(JSON.parse(localStorage.getItem("user")).data.id).then(
      (res) => {
        setRecievedVals(res.data);
      }
    );
  }, []);

  const editUser = (values) => {
    User.updateUserDetails(
      JSON.parse(localStorage.getItem("user")).data.id,
      values
    ).then(() => {
      setOpen(true);
      Auth.logIn(values.email, values.password);
    });
  };

  return (
    <div className="user__info">
      <div className="info__header">
        <ul className="header__list">
          <li>PROFILE</li>
          <li>BILLING</li>
          <li>INVOICE HISTORY</li>
        </ul>
        <div className="header__deactivation"> deactivate account </div>
      </div>
      <div className="info__details">
        <div className="details__left">
          <div className="left__photo">
            <div className="photo__det">
              <span>PROFILE PICTURE</span>
              <div className="photo__det-container">
                <img src={pht} alt="profilePic" />
                <Button variant="contained" color="primary">
                  Upload
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className="details__right">
          <div className="right__det">
            <div className="det__personal">
              <span>Personal Details</span>
              <div className="det__personal-container">
                <Formik
                  enableReinitialize
                  initialValues={{
                    firstName: recievedVals ? recievedVals.firstName : "",
                    lastName: recievedVals ? recievedVals.lastName : "",
                    email: recievedVals ? recievedVals.email : "",
                    password: recievedVals ? recievedVals.password : "",
                  }}
                  onSubmit={editUser}
                  validationSchema={userDetailsScheam}
                >
                  <Form className="inputs__form">
                    <div className="single__input">
                      <Field
                        name="firstName"
                        component={customFirstNameField}
                      />
                    </div>
                    <div className="single__input">
                      <Field name="lastName" component={customLastNameField} />
                    </div>
                    <div className="single__input">
                      <Field name="email" component={customEmailInput} />
                    </div>
                    <div className="password ssd">
                      <Field name="password" component={customPasswordInput} />
                    </div>
                    <Button type="submit" variant="contained" color="primary">
                      Save Changes
                    </Button>
                  </Form>
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="success">
          Updated Successfully!
        </Alert>
      </Snackbar>
    </div>
  );
};
export default UserInfo;
