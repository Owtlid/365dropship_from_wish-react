import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    cursor: "pointer",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  btn: {
    fontSize: 12,
    fontWeight: 600,
  },
}));

export default useStyles;
