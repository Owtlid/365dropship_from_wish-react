import "./LandingPageHeaderNav.css";
import { Link, useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import { Auth } from "../API/Request";
import MenuIcon from "@material-ui/icons/Menu";
import { Button, Drawer } from "@material-ui/core";
import useStyles from "./LandingPageHeaderNav.styles";

const LandingPageHeaderNav = () => {
  const [isAuth, setIsAuth] = useState(false);
  const history = useHistory();

  useEffect(() => {
    if (localStorage.getItem("user") && localStorage.getItem("token")) {
      Auth.checkTokenValidity();
      setIsAuth(true);
    } else {
      setIsAuth(false);
    }
  }, [setIsAuth]);

  const logOut = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    history.go("/");
  };

  const checkToken = () => {
    Auth.checkTokenValidity().then(() => {
      history.go("/catalog");
    });
  };

  const [drawerStatus, setDrawerStatus] = useState(false);

  const classes = useStyles();

  return (
    <>
      <img
        className="CustomIndex__header-logo"
        src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-30.png"
        alt="dropshipLogo"
      />
      <MenuIcon
        fontSize="large"
        color="primary"
        className={classes.root}
        onClick={() => setDrawerStatus(true)}
      />
      <div className="header__nav">
        <Link className="header__nav-item" to="/about">
          <span className="header__nav-item">ABOUT</span>
        </Link>
        <Link className="header__nav-item" to="/catalog">
          <span className="header__nav-item" onClick={checkToken}>
            CATALOG
          </span>
        </Link>
        <Link className="header__nav-item" to="/pricing">
          <span className="header__nav-item">PRICING</span>
        </Link>
        <Link className="header__nav-item" to="/suppliers">
          <span className="header__nav-item">SUPPLIERS</span>
        </Link>
        <Link className="header__nav-item" to="/help">
          <span className="header__nav-item">HELP CENTER</span>
        </Link>
        <Link className="header__nav-item" to="/blog">
          <span className="header__nav-item">BLOG</span>
        </Link>
        {!isAuth && (
          <Link className="header__nav-item" to="/registration">
            <Button className={classes.btn} color="primary" variant="outlined">
              SIGN UP NOW
            </Button>
          </Link>
        )}

        {isAuth ? (
          <Button
            className={classes.btn}
            color="primary"
            variant="outlined"
            onClick={logOut}
          >
            LOG OUT
          </Button>
        ) : (
          <Link className="header__nav-item" to="/login">
            <Button className={classes.btn} color="primary" variant="outlined">
              LOGIN
            </Button>
          </Link>
        )}
        <img
          className="header__nav-logo"
          src="https://webstockreview.net/images/facebook-png-icon-8.png"
          alt="fb"
        />
      </div>
      <Drawer
        open={drawerStatus}
        onClose={() => setDrawerStatus(false)}
        anchor="right"
      >
        <div className="drawer__landing">
          {!isAuth && (
            <Link className="header__nav-item" to="/registration">
              <Button
                className={classes.btn}
                color="primary"
                variant="outlined"
              >
                SIGN UP NOW
              </Button>
            </Link>
          )}

          {isAuth ? (
            <Button
              className={classes.btn}
              color="primary"
              variant="outlined"
              onClick={logOut}
            >
              LOG OUT
            </Button>
          ) : (
            <Link className="header__nav-item" to="/login">
              <Button
                className={classes.btn}
                color="primary"
                variant="outlined"
              >
                LOGIN
              </Button>
            </Link>
          )}
          <img
            className="header__nav-logo"
            src="https://webstockreview.net/images/facebook-png-icon-8.png"
            alt="fb"
          />
        </div>
      </Drawer>
    </>
  );
};
export default LandingPageHeaderNav;
