import "./LandingPage.css";
import LandingPageHeaderNav from "./LandingPageHeaderNav";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { Auth } from "../API/Request";

const LandingPage = () => {
  const [isAuth, setIsAuth] = useState(false);

  useEffect(() => {
    if (localStorage.getItem("user") && localStorage.getItem("token")) {
      Auth.checkTokenValidity();
      setIsAuth(true);
    } else {
      setIsAuth(false);
    }
  }, [setIsAuth]);

  return (
    <div className="CustomIndex">
      <div className="CustomIndex__header">
        <LandingPageHeaderNav />
      </div>
      <div className="CustomIndex__main">
        <img
          src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"
          alt="dropshipLogo"
        ></img>
        <span>WE GOT YOUR SUPPLY CHAIN COVERED</span>
        <span>365 DAYS A YEAR!</span>
      </div>
      {!isAuth && (
        <Link className="header__nav-item" to="/registration">
          <button className="CustomIndex__Button">SIGN UP NOW</button>
        </Link>
      )}
    </div>
  );
};
export default LandingPage;
