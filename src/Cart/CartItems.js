import "./CartItems.css";
import {
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Paper,
  Grid,
} from "@material-ui/core";
import SingleItem from "./SingleItem";
import { useDispatch, useSelector } from "react-redux";
import { changeCartItemsAction } from "../Store/Cart/CartActions";
import { Cart } from "../API/Request";
import { useEffect } from "react";
import useStyles from "./CartItems.styles";
import SingleItemResp from "./SingleItemResp";
import Empty from "../Assets/empty.gif";

const CartItems = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const prodList = useSelector((state) => state.cart.cartItems);

  useEffect(() => {
    Cart.getCart().then((item) => {
      if(item) dispatch(changeCartItemsAction(item.cartItem.items));
    });
  }, [dispatch]);

  return (
    <>
      <div className="cart__items">
        <TableContainer className={classes.container} component={Paper}>
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>
                  <span className="diff">ITEM DESCRIPTION</span>
                </TableCell>
                <TableCell align="right">
                  <span className="diff">SUPPLIER</span>
                </TableCell>
                <TableCell align="right">
                  <span className="diff">QUANTITY</span>
                </TableCell>
                <TableCell align="right">
                  <span className="diff">ITEM COST</span>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {prodList.length > 0
                ? prodList.map((item, i) => {
                    return (
                      <SingleItem
                        image={item.image}
                        title={item.title}
                        price={item.price}
                        id={item.id}
                        key={i}
                      />
                    );
                  })
                : null}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
      <div className="responsive__table">
        <Grid container justify="flex-start">
          {prodList.length ? (
            prodList.map((item, i) => (
              <Grid item md={12} xs={12} key={i}>
                <SingleItemResp
                  image={item.image}
                  title={item.title}
                  price={item.price}
                  id={item.id}
                />
              </Grid>
            ))
          ) : (
            <div className="empty">
              <img src={Empty} alt="emptyGIF" />
            </div>
          )}
        </Grid>
      </div>
    </>
  );
};
export default CartItems;
