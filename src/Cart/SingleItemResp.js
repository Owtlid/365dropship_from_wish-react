import { TextField } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Cart } from "../API/Request";
import { changeCartItemsAction } from "../Store/Cart/CartActions";
import DeleteIcon from "@material-ui/icons/Delete";

const SingleItemResp = ({ image, title, price, id }) => {
  const dispatch = useDispatch();
  const [qty, setQty] = useState(0);

  const cartList = useSelector((state) => state.cart.cartItems);

  useEffect(() => {
    for (let i = 0; i < cartList.length; i++) {
      if (cartList[i].id === id) {
        setQty(cartList[i].qty);
      }
    }
  }, [cartList, id]);

  const changeQuantity = (e) => {
    setQty(e.target.value);
    Cart.updateQuantity(id, e.target.value).then(() => {
      Cart.getCart().then((item) => {
        dispatch(changeCartItemsAction(item.cartItem.items));
      });
    });
  };

  const deleteProd = () => {
    Cart.removeItemFromCart(id).then(() => {
      Cart.getCart().then((item) => {
        dispatch(changeCartItemsAction(item.cartItem.items));
      });
    });
  };

  return (
    <div className="item">
      <div className="item__title">{title}</div>
      <div className="item__img">
        <img src={image} alt="prodImg" />
      </div>
      <div className="item__price">{price}$</div>
      <div className="item__actions">
        <TextField
          type="number"
          variant="outlined"
          value={qty}
          onChange={changeQuantity}
        />
        <DeleteIcon fontSize="large" color="primary" onClick={deleteProd} />
      </div>
    </div>
  );
};
export default SingleItemResp;
