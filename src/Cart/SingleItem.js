import "./SingleItem.css";
import DeleteIcon from "@material-ui/icons/Delete";
import { TableRow, TableCell, TextField } from "@material-ui/core";
import { Cart } from "../API/Request";
import { useEffect, useState } from "react";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { changeCartItemsAction } from "../Store/Cart/CartActions";
import useStyles from "./SingleItem.styles.js";

const SingleItem = ({ image, title, price, id }) => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const classes = useStyles();
  const [qty, setQty] = useState(0);
  const cartList = useSelector((state) => state.cart.cartItems);

  useEffect(() => {
    for (let i = 0; i < cartList.length; i++) {
      if (cartList[i].id === id) {
        setQty(cartList[i].qty);
      }
    }
  }, [cartList, id]);

  const deleteItem = () => {
    Cart.removeItemFromCart(id).then(() => {
      setOpen(true);
      Cart.getCart().then((item) => {
        dispatch(changeCartItemsAction(item.cartItem.items));
      });
    });
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const changeQuantity = (e) => {
    setQty(e.target.value);
    Cart.updateQuantity(id, e.target.value).then(() => {
      Cart.getCart().then((item) => {
        dispatch(changeCartItemsAction(item.cartItem.items));
      });
    });
  };

  return (
    <>
      <TableRow>
        <TableCell component="th">
          <span className="cntr">
            <img src={image} alt="prodImg"></img>
            <span className="diff title">{title}</span>
          </span>
        </TableCell>
        <TableCell align="right">
          <span className="supplier">SP-Supplier115</span>
        </TableCell>
        <TableCell align="right">
          <span className="qty">
            <TextField
              type="number"
              variant="outlined"
              value={qty}
              onChange={changeQuantity}
              className={classes.root}
            />
          </span>
        </TableCell>
        <TableCell align="right">
          <span className="cost">{price}$</span>
          <DeleteIcon fontSize="small" color="primary" onClick={deleteItem} />
        </TableCell>
      </TableRow>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="success">
          Deleted Successfully!
        </Alert>
      </Snackbar>
    </>
  );
};
export default SingleItem;
