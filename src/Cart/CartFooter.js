import "./CartFooter.css";
import { Button } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Cart } from "../API/Request";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { useDispatch } from "react-redux";
import { changeCartItemsAction } from "../Store/Cart/CartActions";

const CartFooter = () => {
  const [totalPrice, setTotalPrice] = useState(0);
  const cartProd = useSelector((state) => state.cart.cartItems);

  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();

  const clearCart = () => {
    Cart.clearCart().then(() => {
      setOpen(true);
      Cart.getCart().then((item) => {
        dispatch(changeCartItemsAction(item.cartItem.items));
      });
    });
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  useEffect(() => {
    if (cartProd) {
      let sum = 0;
      for (let i = 0; i < cartProd.length; i++) {
        sum += cartProd[i].price * cartProd[i].qty;
      }
      setTotalPrice(sum);
    }
  }, [cartProd]);

  return (
    <div className="cart__footer">
      <div className="footer__redirect">
        <Link to="/catalog" className="redircet__btn">
          <Button variant="contained" color="primary">
            continue shopping
          </Button>
        </Link>
      </div>
      <div className="footer__data">
        <div className="footer__data-data">
          <div>
            BALANCE:<span>$0</span>
          </div>
          <div>
            ITEMS TOTAL:<span>${totalPrice}</span>
          </div>
          <div>
            SHIPPING TOTAL:<span>$0</span>
          </div>
          <div>
            ORDER TOTAL:<span>${totalPrice}</span>
          </div>
        </div>
        <div className="footer__data-checkout">
          <Button color="primary" variant="contained" onClick={clearCart}>
            CLEAR CART
          </Button>
          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
          >
            <Alert onClose={handleClose} severity="success">
              Added Successfully!
            </Alert>
          </Snackbar>
          <Button variant="contained" color="primary">
            CHECKOUT
          </Button>
        </div>
      </div>
    </div>
  );
};

export default CartFooter;
