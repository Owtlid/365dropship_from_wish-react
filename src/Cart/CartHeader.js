import "./CartHeader.css";
import HelpOutlineOutlinedIcon from "@material-ui/icons/HelpOutlineOutlined";
import AsideDrawer from "../Catalog/CatalogHeader/AsideDrawer";

const CartHeader = () => {
  return (
    <div className="cart__header">
      <AsideDrawer />
      <span>SHOPPING CART</span>
      <div className="header__about">
        <HelpOutlineOutlinedIcon fontSize="large" color="secondary" />
      </div>
    </div>
  );
};

export default CartHeader;
