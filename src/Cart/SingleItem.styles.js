import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  root: {
    maxWidth: 70,
    border: "none",
  },
}));

export default useStyles;
