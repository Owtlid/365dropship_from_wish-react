import "./Cart.css";
import CartItems from "./CartItems";
import CartHeader from "./CartHeader";
import CartFooter from "./CartFooter";

const Cart = () => {
  return (
    <div className="cart">
      <CartHeader />
      <CartItems />
      <CartFooter />
    </div>
  );
};

export default Cart;
