import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  container: {
    paddingRight: 40,
    paddingLeft: 40,
    boxSizing: "border-box",
  },
}));

export default useStyles;
