import { combineReducers } from "redux";
import cartReducer from "./Cart/CartReducers";
import productsReducer from "./Products/ProductsReducers";

const rootReducers = combineReducers({
  products: productsReducer,
  cart: cartReducer,
});

export default rootReducers;
