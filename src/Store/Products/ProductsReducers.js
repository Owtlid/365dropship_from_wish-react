import * as type from "./ProductsTypes";

const initialState = {
  productsForLogic: [],
  productsForDisplay: [],
  modalId: null,
  sortType: "",
  searchQuery: "",
  filterValues: [],
  singleProductDetails: {},
  addProdModalStatus: false,
  editProdModalId: null,
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case type.PRODUCTS_CHANGED:
      return {
        ...state,
        productsForDisplay: action.payload,
      };
    case type.INITIALISED_PRODUCTS_FOR_LOGIC:
      return {
        ...state,
        productsForLogic: action.payload,
      };
    case type.MODAL_ID_CHANGED:
      return {
        ...state,
        modalId: action.payload,
      };
    case type.SORT_TYPE_CHANGED:
      return {
        ...state,
        sortType: action.payload,
      };
    case type.CHANGED_PRODUCT_AFTER_SORT: {
      return {
        ...state,
        productsForDisplay: action.payload,
      };
    }
    case type.PRODUCT_SELECTED:
      return {
        ...state,
        productsForDisplay: action.payload,
      };
    case type.SELECTED_EVERYTHING:
      return {
        ...state,
        productsForDisplay: action.payload,
      };
    case type.SEARCHED_PRODUCT:
      return {
        ...state,
        productsForDisplay: action.payload,
      };
    case type.SEARCHED_QUERY:
      return {
        ...state,
        searchQuery: action.payload,
      };
    case type.FILTER_VALUES_CHANGED:
      return {
        ...state,
        filterValues: action.payload,
      };
    case type.SINGLE_PRODUCT_CHANGED:
      return {
        ...state,
        singleProductDetails: action.payload,
      };
    case type.CHANGED_ADD_PROD_MODAL_STATUS:
      return {
        ...state,
        addProdModalStatus: action.payload,
      };
    case type.CHANGED_EDIT_PROD_MODAL_ID:
      return {
        ...state,
        editProdModalId: action.payload,
      };
    default:
      return state;
  }
};

export default productsReducer;
