import * as type from "./ProductsTypes";

const addProductsAction = (prodList) => {
  return {
    type: type.PRODUCTS_CHANGED,
    payload: prodList,
  };
};

const initialiseProductsForLogicAction = (prodList) => {
  return {
    type: type.INITIALISED_PRODUCTS_FOR_LOGIC,
    payload: prodList,
  };
};

const changeModalIdAction = (id) => {
  return {
    type: type.MODAL_ID_CHANGED,
    payload: id,
  };
};

const changeSortTypeAction = (str) => {
  return {
    type: type.SORT_TYPE_CHANGED,
    payload: str,
  };
};

const changeSortedProductAction = (data) => {
  return {
    type: type.CHANGED_PRODUCT_AFTER_SORT,
    payload: data,
  };
};

const selectProdAction = (arr) => {
  return {
    type: type.PRODUCT_SELECTED,
    payload: arr,
  };
};

const selectAllAction = (arr) => {
  return {
    type: type.SELECTED_EVERYTHING,
    payload: arr,
  };
};

const searchProdAction = (arr) => {
  return {
    type: type.SEARCHED_PRODUCT,
    payload: arr,
  };
};

const searchQueryAction = (str) => {
  return {
    type: type.SEARCHED_QUERY,
    payload: str,
  };
};

const changeFilterValuesAction = (values) => {
  return {
    type: type.FILTER_VALUES_CHANGED,
    payload: [values[0], values[1]],
  };
};

const changeSingleProductAction = (obj) => {
  return {
    type: type.SINGLE_PRODUCT_CHANGED,
    payload: obj,
  };
};

const changeAddProdModalStatus = (status) => {
  return {
    type: type.CHANGED_ADD_PROD_MODAL_STATUS,
    payload: status,
  };
};

const changeEditProdModalId = (id) => {
  return {
    type: type.CHANGED_EDIT_PROD_MODAL_ID,
    payload: id,
  };
};

export {
  addProductsAction,
  changeModalIdAction,
  changeSortTypeAction,
  selectProdAction,
  selectAllAction,
  searchProdAction,
  initialiseProductsForLogicAction,
  searchQueryAction,
  changeFilterValuesAction,
  changeSingleProductAction,
  changeAddProdModalStatus,
  changeEditProdModalId,
  changeSortedProductAction,
};
