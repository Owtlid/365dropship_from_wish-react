import * as type from "./CartTypes";

const changeCartItemsAction = (itemList) => {
  return {
    type: type.CART_ITEMS_CHANGED,
    payload: itemList,
  };
};

export { changeCartItemsAction };
