import axios from "axios";
import { serverURL } from "./Config";

axios.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.message.includes(401)) {
      clearLocalStorage();
      window.location.href = "/login";
    }
    return Promise.reject(error);
  }
);

const clearLocalStorage = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("user");
};

const setLocalStorage = (user, token) => {
  localStorage.setItem("user", user);
  localStorage.setItem("token", token);
};

const Products = {
  getProducts: async () => {
    try {
      const response = await axios.get(`${serverURL}api/v1/products`);
      return response.data.data;
    } catch (err) {
      alert("vigaca ureeeevs!");
    }
  },
  getSingleProduct: async (id) => {
    try {
      const response = await axios.get(`${serverURL}api/v1/products/${id}`);
      return response.data.data;
    } catch (err) {
      console.log(err);
    }
  },
};

const Auth = {
  registration: async (values) => {
    try {
      const result = await axios.post(serverURL + "register", { ...values });
      setLocalStorage(JSON.stringify(result.data), result.data.data.token);
      window.location.href = "/dashboard";
    } catch (err) {
      console.log(err);
    }
  },
  logIn: async (email, password) => {
    try {
      const result = await axios.post(serverURL + "login", {
        email,
        password,
      });
      setLocalStorage(JSON.stringify(result.data), result.data.data.token);
      window.location.href = "/dashboard";
    } catch (err) {
      alert("account does not exists");
    }
  },
  checkTokenValidity: async () => {
    try {
      const result = await axios.get(serverURL + "api/v1/cart");
      return result;
    } catch (err) {
      throw err;
    }
  },
};

const Cart = {
  getCart: async () => {
    try {
      const result = await axios.get(serverURL + "api/v1/cart");
      return result.data.data;
    } catch (err) {
      console.log(err);
    }
  },
  addItemToCart: async (id) => {
    try {
      const result = await axios.post(serverURL + `api/v1/cart/add`, {
        productId: id,
        qty: 1,
      });
      return result;
    } catch (err) {
      console.log(err);
    }
  },
  updateQuantity: async (id, quant) => {
    try {
      const response = await axios.post(
        serverURL + `api/v1/cart/update/${id}`,
        {
          qty: quant,
        }
      );
      return response;
    } catch (err) {
      console.log(err);
    }
  },
  removeItemFromCart: async (id) => {
    try {
      const response = await axios.post(serverURL + `api/v1/cart/remove/${id}`);
      return response;
    } catch (err) {
      console.log(err);
    }
  },
  clearCart: async () => {
    try {
      const response = await axios.patch(serverURL + "api/v1/cart/clear");
      return response;
    } catch (err) {
      console.log(err);
    }
  },
};

const Admin = {
  addProduct: async (data) => {
    try {
      const response = await axios.post(serverURL + "api/v1/products", data);
      return response.data.data;
    } catch (err) {
      console.log(err);
    }
  },
  updateProduct: async (id, data) => {
    try {
      const response = await axios.put(
        serverURL + `api/v1/products/${id}`,
        data
      );
      return response.data.data;
    } catch (err) {
      alert("cant edit data!");
    }
  },
  deleteProduct: async (id) => {
    try {
      const response = await axios.delete(serverURL + `api/v1/products/${id}`);
      return response.data.data;
    } catch (err) {
      alert("cant delete data!");
    }
  },
};

const User = {
  getSingleUser: async (id) => {
    try {
      const response = await axios.get(serverURL + `api/v1/users/${id}`);
      return response.data;
    } catch (err) {
      alert("erooor simon");
    }
  },
  updateUserDetails: async (id, data) => {
    try {
      const response = await axios.put(serverURL + `api/v1/users/${id}`, {
        ...data,
      });
      return response.data;
    } catch (err) {
      alert("vera heh");
    }
  },
};

export { Admin, Cart, Auth, Products, User };
